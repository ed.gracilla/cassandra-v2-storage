module.exports = {
  fields: {
    co2: {type: 'text'},
    created: {
      type: 'timestamp',
      default: {
        '$db_function': 'toTimestamp(now())'
      }
    },
    data_id: {
      type: 'bigint'
    },
    id: {
      type: 'uuid',
      default: {'$db_function': 'uuid()'}
    },
    is_normal: 'boolean',
    list: {
      type: 'list',
      typeDef: '<varchar>'
    },
    metadata: {
      type: 'map',
      typeDef: '<varchar, text>'
    },
    quality: 'float',
    random_data: 'varchar',
    reading_time: 'timestamp',
    set: {
      type: 'set',
      typeDef: '<varchar>'
    },
    temp: 'int'
  },
  key: [['id'], 'created'],
  clustering_order: {'created': 'DESC'},
  table_name: 'reekohtest12'
}